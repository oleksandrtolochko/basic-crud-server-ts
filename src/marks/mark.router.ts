import express from 'express';
import * as markController from './mark.controller';
import controllerWrapper from '../app/utils/controller-wrapper';

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The mark managing API
 * /api/v1/marks:
 *    get:
 *      summary: Get list of marks
 *      tags: [Marks]
 *      description: Retrieves a list of all marks.
 *      responses:
 *        200:
 *          description: Successful operation
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                items:
 *                  $ref: '#/components/schemas/MarkResponse'
 *        404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: Not Found
 *        500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get('/', controllerWrapper(markController.getAllMarks));

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The mark managing API
 * /api/v1/marks:
 *   post:
 *     summary: Create a new mark
 *     tags: [Marks]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateMarkRequest'
 *     responses:
 *       201:
 *         description: The successfully created mark.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CreatedMarkResponse'
 *       400:
 *         description: Bad request
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Error validating request body. "makr" | "courseId" | "lectorId" | "studentId" is required.
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.post('/', controllerWrapper(markController.postTheMark));

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The mark managing API
 * /api/v1/marks/students/{id}:
 *    get:
 *      summary: Get list marks of the student
 *      tags: [Marks]
 *      parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the student to get.
 *      description: Retrieves all marks of the student by ID.
 *      responses:
 *        200:
 *          description: Successful operation
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                items:
 *                  $ref: '#/components/schemas/MarksOfTheStudentResponse'
 *        404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: Student not found
 *        500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get(
    '/students/:id',
    controllerWrapper(markController.getMarksByStudentId)
);

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The mark managing API
 * /api/v1/marks/courses/{id}:
 *    get:
 *      summary: Get list marks of the course
 *      tags: [Marks]
 *      parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the course to get.
 *      description: Retrieves all marks on the course by ID.
 *      responses:
 *        200:
 *          description: Successful operation
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                items:
 *                  $ref: '#/components/schemas/MarksOfTheCourseResponse'
 *        404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: Course not found
 *        500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get(
    '/courses/:id',
    controllerWrapper(markController.getMarksByCourseId)
);

export default router;
