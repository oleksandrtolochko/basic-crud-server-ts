import express from 'express';
import * as markService from './mark.service';
import { Mark } from './entities/mark.entity';

const getAllMarks = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const marks: Mark[] = await markService.getAllMarks();

    res.status(200).json(marks);
};

const postTheMark = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const { mark, studentId, lectorId, courseId } = req.body;
    const postedMark: Mark = await markService.postTheMark(
        mark,
        studentId,
        lectorId,
        courseId
    );
    res.status(201).json(postedMark);
};

const getMarksByStudentId = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const { id } = req.params;
    if (!id) {
        res.status(400).json('Bad request');
    } else {
        const studentId: number = +id;
        const marks: Mark[] = await markService.getMarksByStudentId(studentId);

        res.status(200).json(marks);
    }
};

const getMarksByCourseId = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const { id } = req.params;
    if (!id) {
        res.status(400).json('Bad request');
    } else {
        const courseId: number = +id;
        const marks: Mark[] = await markService.getMarksByCourseId(courseId);

        res.status(200).json(marks);
    }
};
export { getAllMarks, postTheMark, getMarksByStudentId, getMarksByCourseId };
