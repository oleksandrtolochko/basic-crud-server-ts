import { IMark } from './types/mark.interface';
import Joi from 'joi';

export const markCreateSchema = Joi.object<Omit<IMark, 'id'>>({
    mark: Joi.number().required(),
});
