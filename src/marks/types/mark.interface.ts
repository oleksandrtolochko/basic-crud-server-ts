export interface IMark {
    id: number;
    mark: number;
}

/**
 * @swagger
 * components:
 *  schemas:
 *    MarkResponse:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        createdAt:
 *          type: string
 *          format: date-time
 *        updatedAt:
 *          type: string
 *          format: date-time
 *        mark:
 *          type: number
 *      example:
 *        id: 1
 *        createdAT: "2023-07-15T14:46:52.072Z"
 *        updatedAT: "2023-07-15T14:46:52.072Z"
 *        mark: 4
 *    CreateMarkRequest:
 *      type: object
 *      required:
 *        - mark
 *        - studentId
 *        - lectorId
 *        - courseId
 *      properties:
 *        mark:
 *          type: number
 *        studentId:
 *          type: number
 *        lectorId:
 *          type: number
 *        courseId:
 *          type: number
 *      example:
 *        mark: 4
 *        studentId: 4
 *        lectorId: 1
 *        courseId: 2
 *    CreatedMarkResponse:
 *      type: object
 *      properties:
 *        id:
 *          type: number
 *          example: 2
 *        mark:
 *          type: number
 *          example: 4
 *        student:
 *          $ref: '#/components/schemas/StudentWithGroupId'
 *        lector:
 *          $ref: '#/components/schemas/LectorResponse'
 *        course:
 *          $ref: '#/components/schemas/CourseResponse'
 *        createdAt:
 *          type: string
 *          format: date-time
 *        updatedAt:
 *          type: string
 *          format: date-time
 *    MarksOfTheStudentResponse:
 *      type: object
 *      properties:
 *        mark:
 *          type: number
 *        courseName:
 *          type: string
 *      example:
 *        mark: 4
 *        courseName: "English"
 *    MarksOfTheCourseResponse:
 *      type: object
 *      properties:
 *        coursename:
 *          type: string
 *        lectorname:
 *          type: string
 *        studentname:
 *          type: string
 *        studentsurname:
 *          type: string
 *        mark:
 *          type: number
 *      example:
 *        coursename: "English"
 *        lectorname: "William B"
 *        studentname: "Josh"
 *        studentsurname: "Stanly"
 *        mark: 4
 */
