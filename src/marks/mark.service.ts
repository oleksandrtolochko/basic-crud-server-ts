import { HttpStatuses } from '../app/enums/http-statuses.enum';
import HttpExeption from '../app/exeptions/http-exeption';
import { AppDataSource } from '../configs/database/data-source';
import { Course } from '../courses/entities/course.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { Student } from '../students/entities/student.entity';
import { IStudent } from '../students/types/student.interface';
import { Mark } from './entities/mark.entity';

const markRepository = AppDataSource.getRepository(Mark);
const studentRepository = AppDataSource.getRepository(Student);
const lectorRepository = AppDataSource.getRepository(Lector);
const courseRepository = AppDataSource.getRepository(Course);

const getAllMarks = async (): Promise<Mark[]> => {
    const marks: Mark[] = await markRepository.find();
    return marks;
};

const postTheMark = async (
    mark: number,
    studentId: number,
    lectorId: number,
    courseId: number
): Promise<Mark> => {
    const course: Course | null = await courseRepository.findOneBy({
        id: courseId,
    });

    if (!course) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Course not found');
    }

    const student: Student | null = await studentRepository.findOneBy({
        id: studentId,
    });
    if (!student) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    const lector: Lector | null = await lectorRepository.findOneBy({
        id: lectorId,
    });
    if (!lector) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Lector not found');
    }

    const newMark: Mark = new Mark();
    newMark.course = course;
    newMark.lector = lector;
    newMark.student = student;
    newMark.mark = mark;

    const res = await markRepository.save(newMark);
    return res;
};

const getMarksByStudentId = async (id: number): Promise<Mark[]> => {
    const student: IStudent | null = await studentRepository.findOneBy({ id });

    if (!student) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Student not found');
    }
    const marks: Mark[] = await markRepository
        .createQueryBuilder('mark')
        .select(['course.name AS courseName', 'mark.mark AS mark'])
        .leftJoin('mark.course', 'course')
        .leftJoin('mark.student', 'student')
        .where('student.id = :id', { id })
        .getRawMany();

    return marks;
};

const getMarksByCourseId = async (courseId: number): Promise<Mark[]> => {
    const course = await courseRepository.findOneBy({ id: courseId });

    if (!course) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Course not found');
    }

    const marks: Mark[] = await markRepository
        .createQueryBuilder('mark')
        .select([
            'course.name AS courseName',
            'lector.name AS lectorName',
            'student.name AS studentName',
            'student.surname AS studentSurname',
            'mark.mark AS mark',
        ])
        .leftJoin('mark.course', 'course')
        .leftJoin('mark.lector', 'lector')
        .leftJoin('mark.student', 'student')
        .where('course.id = :courseId', { courseId })
        .getRawMany();

    return marks;
};
export { getAllMarks, postTheMark, getMarksByStudentId, getMarksByCourseId };
