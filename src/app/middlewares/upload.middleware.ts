import path from 'path';
import multer from 'multer';

const multerDestination = path.join(__dirname, '../../', 'tmp');

const multerConfig = multer.diskStorage({
    destination: multerDestination,
    filename: (req, file, callback) => {
        callback(null, file.originalname);
    },
});

const uploadMiddleware = multer({ storage: multerConfig });

export default uploadMiddleware;
