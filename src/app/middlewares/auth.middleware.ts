import basicAuth from 'express-basic-auth';

const USERNAME = process.env.APP_USERNAME as string;
const PASSWORD = process.env.APP_PASSWORD as string;

const auth = basicAuth({
    users: { [USERNAME]: PASSWORD },
});

export default auth;
