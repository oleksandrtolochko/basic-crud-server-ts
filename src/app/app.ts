import express from 'express';
import logger from 'morgan';
import cors from 'cors';
import studentsRouter from '../students/students.router';
import groupRouter from '../groups/group.router';
import lectorRouter from '../lectors/lector.router';
import courseRouter from '../courses/course.router';
import markRouter from '../marks/mark.router';
import exeptionsFilter from './middlewares/exeptions.filter';
import path from 'path';
// import auth from './middlewares/auth.middleware';
import { AppDataSource } from '../configs/database/data-source';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

const app = express();

app.use(logger('tiny'));
app.use(cors());
app.use(express.json());
// app.use(auth);

const options = {
    definition: {
        openapi: '3.1.0',
        info: {
            title: 'University API',
            version: '0.1.0',
            description:
                'This is a simple University API documented with Swagger',
            license: {
                name: 'MIT',
                url: 'https://spdx.org/licenses/MIT.html',
            },
        },
        servers: [
            {
                url: 'http://localhost:3003',
            },
        ],
    },
    apis: ['./**/*.router.ts', './**/*.interface.ts'],
};

const specs = swaggerJsdoc(options);
app.use('/api/v1/api-docs', swaggerUi.serve, swaggerUi.setup(specs));

AppDataSource.initialize()
    .then(() => {
        console.log('Data Source has been initialized!');
    })
    .catch((err) => {
        console.error('Error during Data Source initialization', err);
    });

const staticFilesPath = path.join(__dirname, '../', 'public');
app.use('/api/v1/public', express.static(staticFilesPath));

app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupRouter);
app.use('/api/v1/lectors', lectorRouter);
app.use('/api/v1/courses', courseRouter);
app.use('/api/v1/marks', markRouter);

app.use(exeptionsFilter);

export default app;
