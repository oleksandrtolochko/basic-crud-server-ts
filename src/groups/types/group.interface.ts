export interface IGroup {
    id: number;
    name: string;
    students: [];
}

/**
 * @swagger
 * components:
 *  schemas:
 *    Group:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        createdAt:
 *          type: string
 *          format: date-time
 *        updatedAt:
 *          type: string
 *          format: date-time
 *        name:
 *          type: string
 *        students:
 *          type: array
 *          items:
 *            $ref: '#/components/schemas/StudentWithGroupId'
 *    CreateGroupRequest:
 *      type: object
 *      required:
 *        - name
 *      properties:
 *        name:
 *          type: string
 *    CreatedGroupResponse:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        createdAt:
 *          type: string
 *          format: date-time
 *        updatedAt:
 *          type: string
 *          format: date-time
 *        name:
 *          type: string
 *        students:
 *          type: array
 *          nullable: true
 */
