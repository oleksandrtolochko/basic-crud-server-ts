import { HttpStatuses } from '../app/enums/http-statuses.enum';
import HttpExeption from '../app/exeptions/http-exeption';
import { IGroup } from './types/group.interface';
import { Group } from './entities/group.entity';
import { AppDataSource } from '../configs/database/data-source';
import { DeleteResult, UpdateResult } from 'typeorm';

const groupRepository = AppDataSource.getRepository(Group);

const getAllGroups = async (): Promise<Group[]> => {
    const groups: Group[] = await groupRepository.find({
        relations: ['students'],
        order: {
            id: 'ASC',
        },
    });

    return groups;
};

const getGroupById = async (groupId: number): Promise<Group | null> => {
    const group: Group | null = await groupRepository
        .createQueryBuilder('group')
        .leftJoinAndSelect('group.students', 'student')
        .where('group.id = :groupId', { groupId })
        .getOne();

    if (!group) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Group not found');
    }

    return group;
};

const addNewGroup = async (
    groupCreateSchema: Omit<IGroup, 'id'>
): Promise<Group> => {
    const group: Group | null = await groupRepository.findOneBy({
        name: groupCreateSchema.name,
    });

    if (group) {
        throw new HttpExeption(HttpStatuses.BAD_REQUEST, 'Group already exist');
    }

    const newGroup: Omit<IGroup, 'id'> & Group = await groupRepository.save(
        groupCreateSchema
    );
    return newGroup;
};

const updateGroupById = async (
    groupId: number,
    groupUpdateSchema: Partial<IGroup>
): Promise<UpdateResult> => {
    const res: UpdateResult = await groupRepository
        .createQueryBuilder()
        .update(Group)
        .set(groupUpdateSchema)
        .where('id = :id', { id: groupId })
        .execute();

    if (!res.affected) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Group not found');
    }
    return res;
};

const deleteGroupById = async (groupId: number): Promise<DeleteResult> => {
    const res: DeleteResult = await groupRepository.delete({ id: groupId });
    if (!res.affected) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Group not found');
    }
    return res;
};

export {
    getAllGroups,
    getGroupById,
    addNewGroup,
    updateGroupById,
    deleteGroupById,
};
