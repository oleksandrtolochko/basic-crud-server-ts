import express from 'express';
import * as groupService from './group.service';
import { Group } from './entities/group.entity';

const getAllGroups = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const groups: Group[] = await groupService.getAllGroups();

    res.status(200).json(groups);
};

const getGroupById = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const groupId: number = +req.params.id;
    const group: Group | null = await groupService.getGroupById(groupId);

    res.status(200).json(group);
};

const addNewGroup = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const data = req.body;
    const newGroup: Group = await groupService.addNewGroup(data);
    res.status(201).json(newGroup);
};

const updateGrouptById = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const id: number = +req.params.id;
    const data = req.body;
    await groupService.updateGroupById(id, data);
    res.status(200).json('Group successfully updated');
};

const deleteGroupById = async (
    req: express.Request<{ id: number }>,
    res: express.Response
): Promise<void> => {
    const groupId: number = +req.params.id;
    await groupService.deleteGroupById(groupId);
    res.status(200).json({ message: 'group successfully deleted' });
};

export {
    getAllGroups,
    getGroupById,
    addNewGroup,
    updateGrouptById,
    deleteGroupById,
};
