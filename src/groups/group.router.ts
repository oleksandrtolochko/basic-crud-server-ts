import express from 'express';
import * as groupController from './group.controller';
import controllerWrapper from '../app/utils/controller-wrapper';
import validator from '../app/middlewares/validation.middlevare';
import { groupCreateSchema, groupUpdateSchema } from './group.schema';
import { idParamSchema } from '../app/schemas/id-param.schema';

const router = express.Router();
/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The group managing API
 * /api/v1/groups:
 *    get:
 *      summary: Get all groups with their students
 *      tags: [Groups]
 *      description: Retrieves a list of all groups.
 *      responses:
 *        200:
 *          description: Successful operation
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                items:
 *                  $ref: '#/components/schemas/Group'
 *        404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: Not Found
 *        500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get('/', controllerWrapper(groupController.getAllGroups));

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The group managing API
 * /api/v1/groups/{id}:
 *    get:
 *      summary: Get group by ID with students
 *      tags: [Groups]
 *      parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the group to get.
 *      description: Retrieves group by ID.
 *      responses:
 *        200:
 *          description: Successful operation
 *          content:
 *            application/json:
 *              schema:
 *                  $ref: '#/components/schemas/Group'
 *        404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: Not Found
 *        500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(groupController.getGroupById)
);
/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The group managing API
 * /api/v1/groups:
 *   post:
 *     summary: Create a new group
 *     tags: [Groups]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateGroupRequest'
 *     responses:
 *       201:
 *         description: The successfully created group.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CreatedGroupResponse'
 *       400:
 *         description: Bad request
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Error validating request body. "name" is required.
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.post(
    '/',
    validator.body(groupCreateSchema),
    controllerWrapper(groupController.addNewGroup)
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The group managing API
 * /api/v1/groups/{id}:
 *   patch:
 *     summary: Update the group
 *     tags: [Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the group to update.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateGroupRequest'
 *     responses:
 *       200:
 *         description: message about success.
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: "Group successfully updated"
 *       400:
 *         description: Bad request
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Error validating request body. "students" is not allowed.
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: number
 *                   description: error status code
 *                 message:
 *                   type: string
 *                   description: error message
 *               example:
 *                 status: 404
 *                 message: "Group not found"
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.patch(
    '/:id',
    validator.params(idParamSchema),
    validator.body(groupUpdateSchema),
    controllerWrapper(groupController.updateGrouptById)
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The group managing API
 * /api/v1/groups/{id}:
 *   delete:
 *     summary: Delete group by id.
 *     tags: [Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the group to delete.
 *     responses:
 *       200:
 *         description: Message about successfful deleting.
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: "Successfully deleted"
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: number
 *                   description: error status code
 *                 message:
 *                   type: string
 *                   description: error message
 *               example:
 *                 status: 404
 *                 message: "Group not found"
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.delete(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(groupController.deleteGroupById)
);

export default router;
