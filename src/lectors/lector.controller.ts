import express from 'express';
import * as lectorService from './lector.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ILectorCreateRequest } from './types/lector-create-request.interface';
import { Lector } from './entities/lector.entity';
import { ILector } from './types/lector.interface';

const getAllLectors = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const lectors: Lector[] = await lectorService.getAllLectors();

    res.status(200).json(lectors);
};

const addNewLector = async (
    req: ValidatedRequest<ILectorCreateRequest>,
    res: express.Response
): Promise<void> => {
    const data: Omit<ILector, 'id'> = req.body;
    const newLector: Lector = await lectorService.addNewLector(data);

    res.status(201).json(newLector);
};

const subscribeLectorToCourse = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const courseId: number = req.body?.id;
    const { id } = req.params;
    const lectorId: number = +id;

    await lectorService.subscribeLectorToCourse(lectorId, courseId);

    res.status(200).json('Lector successfylly added to the course');
};

const getLectorById = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const { id } = req.params;
    const lectorId: number = +id;
    const lector: Lector | null = await lectorService.getLectorById(lectorId);
    res.status(200).json(lector);
};

const getCoursesByLectorId = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const { id } = req.params;
    const lectorId: number = +id;
    const lector: Lector | null = await lectorService.getCoursesByLectorId(
        lectorId
    );
    if (!lector) {
        res.status(404).json(
            'Sorry, at the moment the indicated lecturer does not teach any of the courses'
        );
    }
    res.status(200).json(lector);
};

export {
    getAllLectors,
    addNewLector,
    subscribeLectorToCourse,
    getLectorById,
    getCoursesByLectorId,
};
