export interface ILector {
    id: number;
    name: string;
    email: string;
    password: string;
}

/**
 * @swagger
 * components:
 *  schemas:
 *    LectorResponse:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        createdAt:
 *          type: string
 *          format: date-time
 *        updatedAt:
 *          type: string
 *          format: date-time
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        password:
 *          type: string
 *        example:
 *          id: 1
 *          createdAT: "2023-07-15T14:46:52.072Z"
 *          updatedAT: "2023-07-15T14:46:52.072Z"
 *          name: 'Bob'
 *          email: 'example@mail.com'
 *          password: 'qwerty1234'
 *    LectorResponseWithCourses:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        createdAt:
 *          type: string
 *          format: date-time
 *        updatedAt:
 *          type: string
 *          format: date-time
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        password:
 *          type: string
 *        courses:
 *          type: array
 *          items:
 *            $ref: '#/components/schemas/CourseResponse'
 *    CreateLectorRequest:
 *      type: object
 *      required:
 *        - name
 *        - email
 *        - password
 *      properties:
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        password:
 *          type: string
 *    CreatedLectorResponse:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        createdAt:
 *          type: string
 *          format: date-time
 *        updatedAt:
 *          type: string
 *          format: date-time
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        password:
 *          type: string
 */
