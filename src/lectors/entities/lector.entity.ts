import { Column, Entity, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { CoreEntity } from '../../app/entities/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'lectors' })
export class Lector extends CoreEntity {
    @Column({
        type: 'varchar',
    })
    name: string;

    @Column({
        type: 'varchar',
    })
    email: string;

    @Column({
        type: 'varchar',
    })
    password: string;

    @ManyToMany(() => Course, (course) => course.lectors)
    @JoinTable({
        name: 'lector_course',
        joinColumn: {
            name: 'lector_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'course_id',
            referencedColumnName: 'id',
        },
    })
    courses: Course[];

    @OneToMany(() => Mark, (mark) => mark.lector)
    marks: Mark[];
}
