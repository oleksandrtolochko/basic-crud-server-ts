import { HttpStatuses } from '../app/enums/http-statuses.enum';
import HttpExeption from '../app/exeptions/http-exeption';
import { AppDataSource } from '../configs/database/data-source';
import { Lector } from './entities/lector.entity';
import { ILector } from './types/lector.interface';
import { Course } from '../courses/entities/course.entity';

const lectorRepository = AppDataSource.getRepository(Lector);
const courseRepository = AppDataSource.getRepository(Course);

const getAllLectors = async (): Promise<Lector[]> => {
    const lectors: Lector[] = await lectorRepository.find({});
    return lectors;
};

const addNewLector = async (
    lectorCreateSchema: Omit<ILector, 'id'>
): Promise<Lector> => {
    const lector: Lector | null = await lectorRepository.findOneBy({
        name: lectorCreateSchema.name,
    });

    if (lector) {
        throw new HttpExeption(
            HttpStatuses.BAD_REQUEST,
            'Lector already exist'
        );
    }

    const newLector: Omit<ILector, 'id'> & Lector = await lectorRepository.save(
        lectorCreateSchema
    );
    return newLector;
};

const subscribeLectorToCourse = async (
    lectorId: number,
    courseId: number
): Promise<void> => {
    const course: Course | null = await courseRepository.findOneBy({
        id: courseId,
    });

    if (!course) {
        throw new HttpExeption(
            HttpStatuses.BAD_REQUEST,
            "Course doesn't exist"
        );
    }

    const lector: Lector | null = await lectorRepository.findOneBy({
        id: lectorId,
    });

    if (!lector) {
        throw new HttpExeption(
            HttpStatuses.BAD_REQUEST,
            "Lector doesn't exist"
        );
    }

    try {
        await lectorRepository
            .createQueryBuilder()
            .relation(Lector, 'courses')
            .of(lectorId)
            .add(courseId);
    } catch (error) {
        throw new HttpExeption(HttpStatuses.INTERNAL_SERVER_ERROR, `${error}`);
    }
};

const getLectorById = async (lectorId: number): Promise<Lector | null> => {
    const lector: Lector | null = await lectorRepository
        .createQueryBuilder('lector')
        .leftJoinAndSelect('lector.courses', 'course')
        .leftJoinAndSelect('course.students', 'student')
        .where('lector.id = :id', { id: lectorId })
        .getOne();

    if (!lector) {
        throw new Error('Lector not found');
    }

    return lector;
};

const getCoursesByLectorId = async (
    lectorId: number
): Promise<Lector | null> => {
    const lector: Lector | null = await lectorRepository
        .createQueryBuilder('lector')
        .leftJoinAndSelect('lector.courses', 'course')
        .where('lector.id = :lectorId', { lectorId })
        .getOne();

    if (!lector) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Lector not found');
    }

    if (!lector.courses.length) {
        return null;
    }
    return lector;
};

export {
    getAllLectors,
    addNewLector,
    subscribeLectorToCourse,
    getLectorById,
    getCoursesByLectorId,
};
