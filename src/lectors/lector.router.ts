import express from 'express';
import * as lectorController from './lector.controller';
import controllerWrapper from '../app/utils/controller-wrapper';
import validator from '../app/middlewares/validation.middlevare';
import { idParamSchema } from '../app/schemas/id-param.schema';
import { lectorCreateSchema } from './lector.schema';
import { courseUpdateLectorsSchema } from '../courses/course.schema';

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lector managing API
 * /api/v1/lectors:
 *    get:
 *      summary: Get list of lectors
 *      tags: [Lectors]
 *      description: Retrieves a list of all lectors.
 *      responses:
 *        200:
 *          description: Successful operation
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                items:
 *                  $ref: '#/components/schemas/LectorResponse'
 *        404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: Not Found
 *        500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get('/', controllerWrapper(lectorController.getAllLectors));

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lector managing API
 * /api/v1/lectors/{id}:
 *    get:
 *      summary: Get lector by ID with courses
 *      tags: [Lectors]
 *      parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the lector to get.
 *      description: Retrieves lector by ID.
 *      responses:
 *        200:
 *          description: Successful operation
 *          content:
 *            application/json:
 *              schema:
 *                  $ref: '#/components/schemas/LectorResponseWithCourses'
 *        404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: Not Found
 *        500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(lectorController.getLectorById)
);
/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lector managing API
 * /api/v1/lectors/{id}/courses:
 *    get:
 *      summary: Get lector by ID with all courses
 *      tags: [Lectors]
 *      parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the lector to get.
 *      description: Retrieves lector by ID with courses.
 *      responses:
 *        200:
 *          description: Successful operation
 *          content:
 *            application/json:
 *              schema:
 *                  $ref: '#/components/schemas/LectorResponseWithCourses'
 *        404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: 'Sorry, at the moment the indicated lecturer does not teach any of the courses'
 *        500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get(
    '/:id/courses',
    validator.params(idParamSchema),
    controllerWrapper(lectorController.getCoursesByLectorId)
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lector managing API
 * /api/v1/lectors:
 *   post:
 *     summary: Create a new lector
 *     tags: [Lectors]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateLectorRequest'
 *     responses:
 *       201:
 *         description: The successfully created lector.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CreatedLectorResponse'
 *       400:
 *         description: Bad request
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Error validating request body. "name" | "email" | "password" is required.
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.post(
    '/',
    validator.body(lectorCreateSchema),
    controllerWrapper(lectorController.addNewLector)
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lector managing API
 * /api/v1/lectors/{id}/courses:
 *    patch:
 *      summary: Update lector by ID => subscribe lector to the course
 *      tags: [Lectors]
 *      parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the lector to update.
 *      description: Retrieves lector by ID with courses.
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                id:
 *                  type: number
 *                  description: course id
 *              example:
 *                id: 2
 *      responses:
 *        200:
 *          description: Successful operation
 *          content:
 *            application/json:
 *              schema:
 *              type: string
 *              example: Lector successfylly added to the course
 *        404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: Not Found
 *        500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.patch(
    '/:id/courses',
    validator.params(idParamSchema),
    validator.body(courseUpdateLectorsSchema),
    controllerWrapper(lectorController.subscribeLectorToCourse)
);

export default router;
