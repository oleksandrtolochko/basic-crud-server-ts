import express from 'express';
import * as studentsController from './students.controller';
import controllerWrapper from '../app/utils/controller-wrapper';
import validator from '../app/middlewares/validation.middlevare';
import {
    studentCreateSchema,
    studentGroupUpdateSchema,
    studentUpdateSchema,
} from './student.schema';
import { idParamSchema } from '../app/schemas/id-param.schema';
// import uploadMiddleware from '../app/middlewares/upload.middleware';

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The student managing API
 * /api/v1/students/?:
 *   get:
 *     summary: Get list of students.
 *     tags: [Students]
 *     parameters:
 *      - in: query
 *        name: name
 *        type: string
 *        description: To get the list of students with Query name.
 *     responses:
 *       200:
 *         description: The list of students.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                  $ref: '#/components/schemas/Student'
 *       404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: Not Found
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get('/?', controllerWrapper(studentsController.getStudents));

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The student managing API
 * /api/v1/students/{id}:
 *   get:
 *     summary: Get student by id.
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the student to get.
 *     responses:
 *       200:
 *         description: The student by ID.
 *         content:
 *           application/json:
 *             schema:
 *                  $ref: '#/components/schemas/Student'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: number
 *                   description: error status code
 *                 message:
 *                   type: string
 *                   description: error message
 *               example:
 *                 status: 404
 *                 message: "Student not found"
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(studentsController.getStudentById)
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The student managing API
 * /api/v1/students:
 *   post:
 *     summary: Create a new student
 *     tags: [Students]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateStudent'
 *     responses:
 *       201:
 *         description: The created student.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CreatedStudent'
 *       400:
 *         description: Bad request
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Error validating request body. "name"|"surname"|"email"|"age" is required.
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.post(
    '/',
    validator.body(studentCreateSchema),
    controllerWrapper(studentsController.addNewStudent)
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The student managing API
 * /api/v1/students/{id}:
 *   patch:
 *     summary: Update the student
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the student to update.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdateStudent'
 *     responses:
 *       200:
 *         description: message about success.
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: "Student successfully updated"
 *       400:
 *         description: Bad request
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Error validating request body. "groupId" is not allowed.
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: number
 *                   description: error status code
 *                 message:
 *                   type: string
 *                   description: error message
 *               example:
 *                 status: 404
 *                 message: "Student not found"
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.patch(
    '/:id',
    validator.params(idParamSchema),
    validator.body(studentUpdateSchema),
    controllerWrapper(studentsController.updateStudentById)
);

// router.patch(
//     '/:id/image',
//     validator.params(idParamSchema),
//     uploadMiddleware.single('file'),
//     controllerWrapper(studentsController.addImage)
// );
/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The student managing API
 * /api/v1/students/{id}/group:
 *   patch:
 *     summary: Update the student group
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the student to get.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdateStudentGroup'
 *     responses:
 *       200:
 *         description: The updated student group.
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: "Student successfully added to the group"
 *       400:
 *         description: Bad request
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Error validating request body. "name"|"surname"|"email"|"age" is not allowed.
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: number
 *                   description: error status code
 *                 message:
 *                   type: string
 *                   description: error message
 *               example:
 *                 status: 404
 *                 message: "Student not found"
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.patch(
    '/:id/group',
    validator.params(idParamSchema),
    validator.body(studentGroupUpdateSchema),
    controllerWrapper(studentsController.updateStudentGroup)
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The student managing API
 * /api/v1/students/{id}:
 *   delete:
 *     summary: Delete student by id.
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         type: integer
 *         required: true
 *         description: Numeric ID of the student to delete.
 *     responses:
 *       200:
 *         description: Message about successfful deleting.
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: "Successfully deleted"
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: number
 *                   description: error status code
 *                 message:
 *                   type: string
 *                   description: error message
 *               example:
 *                 status: 404
 *                 message: "Student not found"
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.delete(
    '/:id',
    validator.params(idParamSchema),
    controllerWrapper(studentsController.deleteStudentById)
);

export default router;
