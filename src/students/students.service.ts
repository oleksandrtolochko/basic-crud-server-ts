// import ObjectID from 'bson-objectid';
import { HttpStatuses } from '../app/enums/http-statuses.enum';
import HttpExeption from '../app/exeptions/http-exeption';
import { IStudent } from './types/student.interface';
// import path from 'path';
// import fs from 'fs/promises';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { DeleteResult, UpdateResult } from 'typeorm';

const studentRepository = AppDataSource.getRepository(Student);

const getAllStudents = async (): Promise<Student[]> => {
    const students: Student[] = await studentRepository
        .createQueryBuilder('student')
        .select([
            'student.id as id',
            'student.name as name',
            'student.surname as surname',
            'student.email as email',
            'student.age as age',
        ])
        .leftJoin('student.group', 'group')
        .addSelect('group.name as "groupName"')
        .getRawMany();

    return students;
};

const getStudentById = async (studentId: number): Promise<Student> => {
    const student: Student | undefined = await studentRepository
        .createQueryBuilder('student')
        .select([
            'student.id as id',
            'student.name as name',
            'student.surname as surname',
            'student.email as email',
            'student.age as age',
        ])
        .leftJoin('student.group', 'group')
        .addSelect('group.name as "groupName"')
        .where('student.id = :studentId', { studentId })
        .getRawOne();

    if (!student) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    return student;
};

const getStudentsByName = async (studentName: string): Promise<Student[]> => {
    const searchName: string = studentName.toLowerCase();
    const students: Student[] = await studentRepository
        .createQueryBuilder('student')
        .select([
            'student.id as id',
            'student.name as name',
            'student.surname as surname',
            'student.email as email',
            'student.age as age',
        ])
        .leftJoin('student.group', 'group')
        .addSelect('group.name as "groupName"')
        .where('LOWER(student.name) = :searchName', { searchName })
        .getRawMany();

    if (!students) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    return students;
};

const addNewStudent = async (
    studentCreateSchema: Omit<IStudent, 'id'>
): Promise<Student> => {
    const student: Student | null = await studentRepository.findOneBy({
        email: studentCreateSchema.email,
    });

    if (student) {
        throw new HttpExeption(
            HttpStatuses.BAD_REQUEST,
            'Student already exist'
        );
    }

    const newStudent: Omit<IStudent, 'id'> & Student =
        await studentRepository.save(studentCreateSchema);
    return newStudent;
};

const updateStudentById = async (
    studentId: number,
    studentUpdateSchema: Partial<IStudent>
): Promise<UpdateResult> => {
    const res: UpdateResult = await studentRepository
        .createQueryBuilder()
        .update(Student)
        .set(studentUpdateSchema)
        .where('id = :id', { id: studentId })
        .execute();

    if (!res.affected) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Student not found');
    }
    return res;
};

// const addImage = async (id: string, filePath?: string) => {
//     if (!filePath) {
//         throw new HttpExeption(
//             HttpStatuses.BAD_REQUEST,
//             'File is not provided'
//         );
//     }

//     try {
//         const imageId = ObjectID().toHexString();

//         const imageExtention = path.extname(filePath);

//         const imageName = imageId + imageExtention;

//         const studentsDirectoryName = 'students';
//         const studentsDirectoryPath = path.join(
//             __dirname,
//             '../',
//             'public/',
//             studentsDirectoryName
//         );
//         const newImagePath = path.join(studentsDirectoryPath, imageName);
//         const imagePath = `${studentsDirectoryName}/${imageName}`;

//         await fs.rename(filePath, newImagePath);

//         const updatedStudent = updateStudentById(id, { imagePath });
//         return updatedStudent;
//     } catch (error) {
//         await fs.unlink(filePath);
//         throw error;
//     }
// };

const deleteStudentById = async (studentId: number): Promise<DeleteResult> => {
    const res: DeleteResult = await studentRepository.delete({ id: studentId });

    if (!res.affected) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    return res;
};

export {
    getAllStudents,
    getStudentById,
    getStudentsByName,
    addNewStudent,
    updateStudentById,
    // addImage,
    deleteStudentById,
};
