import {
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToMany,
    ManyToOne,
    OneToMany,
} from 'typeorm';
import { CoreEntity } from '../../app/entities/core.entity';
import { Group } from '../../groups/entities/group.entity';
import { Course } from '../../courses/entities/course.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'students' })
@Index(['name'])
export class Student extends CoreEntity {
    @Column({
        type: 'varchar',
    })
    name: string;

    @Column({
        type: 'varchar',
    })
    surname: string;

    @Column({
        type: 'varchar',
    })
    email: string;

    @Column({
        type: 'numeric',
    })
    age: number;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    imagePath: string;

    @ManyToOne(() => Group, (group) => group.students, {
        nullable: true,
        eager: false,
    })
    @JoinColumn({ name: 'group_id' })
    group: Group;

    @Column({
        type: 'integer',
        nullable: true,
        name: 'group_id',
    })
    groupId: number;

    @ManyToMany(() => Course, (course) => course.students)
    courses: Course[];

    @OneToMany(() => Mark, (mark) => mark.student)
    marks: Mark[];
}
