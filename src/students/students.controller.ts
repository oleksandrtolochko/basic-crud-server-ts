import express from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IstudentCreateRequest } from './types/student-create-request.interface';
import { IstudentUpdateRequest } from './types/student-update-request.interface';
import { Student } from './entities/student.entity';

const getStudents = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const studentName: string | undefined = req.query.name as string;

    if (req.query.name) {
        const students: Student[] = await studentsService.getStudentsByName(
            studentName
        );
        res.status(200).json(students);
    } else {
        const students: Student[] = await studentsService.getAllStudents();
        res.status(200).json(students);
    }
};

const getStudentById = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const studentId = +req.params.id;
    const student = await studentsService.getStudentById(studentId);
    res.status(200).json(student);
};

const addNewStudent = async (
    req: ValidatedRequest<IstudentCreateRequest>,
    res: express.Response
): Promise<void> => {
    const data = req.body;
    const newStudent = await studentsService.addNewStudent(data);
    res.status(201).json(newStudent);
};

const updateStudentById = async (
    req: ValidatedRequest<IstudentUpdateRequest>,
    res: express.Response
): Promise<void> => {
    const studentId = req.params.id;
    const info = req.body;
    await studentsService.updateStudentById(studentId, info);
    res.status(200).json('Successfully updated');
};

// const addImage = async (
//     req: express.Request<{ id: string; file: Express.Multer.File }>,
//     res: express.Response
// ) => {
//     const studentId = req.params.id;
//     const { path } = req.file ?? {};
//     const updatedStudent = await studentsService.addImage(studentId, path);
//     res.status(200).json(updatedStudent);
// };

const updateStudentGroup = async (
    req: ValidatedRequest<IstudentUpdateRequest>,
    res: express.Response
): Promise<void> => {
    const studentId = req.params.id;
    const groupId = req.body;

    await studentsService.updateStudentById(studentId, groupId);
    res.status(200).json('Student successfully added to the group');
};

const deleteStudentById = async (
    req: express.Request<{ id: number }>,
    res: express.Response
): Promise<void> => {
    const studentId = req.params.id;
    await studentsService.deleteStudentById(studentId);
    res.status(200).json({ message: 'student successfully deleted' });
};

export {
    getStudents,
    getStudentById,
    addNewStudent,
    updateStudentById,
    // addImage,
    updateStudentGroup,
    deleteStudentById,
};
