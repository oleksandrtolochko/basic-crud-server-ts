import { IStudent } from './types/student.interface';
import ObjectID from 'bson-objectid';

export const students: IStudent[] = [
    {
        id: ObjectID().toHexString(),
        name: 'Bob',
        surname: 'White',
        email: 'BWhite@mail.com',
        age: 21,
        groupId: null,
    },
    {
        id: ObjectID().toHexString(),
        name: 'Jonny',
        surname: 'Rocket',
        email: 'JoD@mail.com',
        age: 22,
        groupId: null,
    },
    {
        id: ObjectID().toHexString(),
        name: 'Jonny',
        surname: 'Pops',
        email: 'JoD@mail.com',
        age: 22,
        groupId: null,
    },
    {
        id: ObjectID().toHexString(),
        name: 'Jonny',
        surname: 'Snow',
        email: 'JoD@mail.com',
        age: 22,
        groupId: null,
    },
    {
        id: ObjectID().toHexString(),
        name: 'Jonny',
        surname: 'Bredly',
        email: 'JoD@mail.com',
        age: 22,
        groupId: null,
    },
];

const getAllStudents = (): IStudent[] => {
    return students;
};

const getStudentById = (studentId: string): IStudent | undefined => {
    const student = students.find(({ id }) => id === studentId);
    return student;
};

const addNewStudent = (createStudentSchema: Omit<IStudent, 'id'>): IStudent => {
    const newStudent = {
        ...createStudentSchema,
        id: ObjectID().toHexString(),
    };
    students.push(newStudent);
    return newStudent;
};

const updateStudentById = (
    studentId: string,
    updateStudentSchema: Partial<IStudent>
): IStudent | undefined => {
    const studentIndex = students.findIndex(({ id }) => id === studentId);
    const student = students[studentIndex];

    if (!student) {
        return;
    }

    const updatedStudent = {
        ...student,
        ...updateStudentSchema,
    };

    students.splice(studentIndex, 1, updatedStudent);

    return updatedStudent;
};

const deleteStudentById = (studentId: string): IStudent | undefined => {
    const studentIndex = students.findIndex(({ id }) => id === studentId);
    const student = students[studentIndex];

    if (!student) {
        return;
    }

    students.splice(studentIndex, 1);

    return student;
};

export {
    getAllStudents,
    getStudentById,
    addNewStudent,
    updateStudentById,
    deleteStudentById,
};
