import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './student.interface';

export interface IstudentUpdateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Partial<IStudent>;
}

/**
 * @swagger
 * components:
 *   schemas:
 *     UpdateStudent:
 *       type: object
 *       optional:
 *         - name
 *         - surname
 *         - email
 *         - age
 *       properties:
 *         name:
 *           type: string
 *           description: name of the student
 *         surname:
 *           type: string
 *           description: surname of the student
 *         email:
 *           type: string
 *           description: student's email
 *         age:
 *           type: number
 *           description: student's age
 *       example:
 *         name: Bob
 *         surname: Dewdney
 *         email: example@mail.com
 *         age: 21
 *         groupId: 1
 *     UpdateStudentGroup:
 *       type: object
 *       required:
 *         - groupId
 *       properties:
 *         groupId:
 *           type: string
 *           description: group ID
 *       example:
 *         groupId: "1"
 */
