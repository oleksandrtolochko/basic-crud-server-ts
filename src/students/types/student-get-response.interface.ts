import { IStudent } from './student.interface';

export interface IStudentGetResponse extends IStudent {
    createdAt: Date;
    updatedAt: Date;
}

/**
 * @swagger
 * components:
 *   schemas:
 *     Student:
 *       type: object
 *       properties:
 *         id:
 *           type: number
 *           description: student id
 *         name:
 *           type: string
 *           description: name of the student
 *         surname:
 *           type: string
 *           description: surname of the student
 *         email:
 *           type: string
 *           description: student's email
 *         age:
 *           type: number
 *           description: student's age
 *         groupName:
 *           type: string
 *           nullable: true
 *           description: student's group name
 *         imagePath:
 *           type: string
 *           nullable: true
 *           description: student's group name
 *         createdAT:
 *           type: date-time
 *           descriptopn: student creating date and time
 *         updatedAT:
 *           type: date-time
 *           descriptopn: student updating date and time
 *       example:
 *         id: 1
 *         name: Bob
 *         surname: Dewdney
 *         email: example@mail.com
 *         age: 21
 *         imagePath: null
 *         groupName: MIT-2
 *         createdAT: "2023-07-15T14:46:52.072Z"
 *         updatedAT: "2023-07-15T14:46:52.072Z"
 *     StudentWithGroupId:
 *       type: object
 *       properties:
 *         id:
 *           type: number
 *           description: student id
 *         name:
 *           type: string
 *           description: name of the student
 *         surname:
 *           type: string
 *           description: surname of the student
 *         email:
 *           type: string
 *           description: student's email
 *         age:
 *           type: number
 *           description: student's age
 *         groupId:
 *           type: string
 *           nullable: true
 *           description: student's group ID
 *         imagePath:
 *           type: string
 *           nullable: true
 *           description: student's group name
 *         createdAT:
 *           type: date-time
 *           descriptopn: student creating date and time
 *         updatedAT:
 *           type: date-time
 *           descriptopn: student updating date and time
 *       example:
 *         id: 1
 *         name: Bob
 *         surname: Dewdney
 *         email: example@mail.com
 *         age: 21
 *         imagePath: null
 *         groupId: 2
 *         createdAT: "2023-07-15T14:46:52.072Z"
 *         updatedAT: "2023-07-15T14:46:52.072Z"
 *     CreatedStudent:
 *       type: object
 *       properties:
 *         id:
 *           type: number
 *           description: student id
 *         name:
 *           type: string
 *           description: name of the student
 *         surname:
 *           type: string
 *           description: surname of the student
 *         email:
 *           type: string
 *           description: student's email
 *         age:
 *           type: number
 *           description: student's age
 *         groupName:
 *           type: string
 *           nullable: true
 *           description: student's group name
 *         imagePath:
 *           type: string
 *           nullable: true
 *           description: student's group name
 *         createdAT:
 *           type: date-time
 *           descriptopn: student creating date and time
 *         updatedAT:
 *           type: date-time
 *           descriptopn: student updating date and time
 *       example:
 *         id: 1
 *         name: Bob
 *         surname: Dewdney
 *         email: example@mail.com
 *         age: 21
 *         groupName: MIT-2
 *         createdAT: "2023-07-15T14:46:52.072Z"
 *         updatedAT: "2023-07-15T14:46:52.072Z"
 *
 */
