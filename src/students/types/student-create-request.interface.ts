import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './student.interface';

export interface IstudentCreateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Omit<IStudent, 'id'>;
}

/**
 * @swagger
 * components:
 *   schemas:
 *     CreateStudent:
 *       type: object
 *       required:
 *         - name
 *         - surname
 *         - email
 *         - age
 *       optional:
 *         - groupID
 *       properties:
 *         name:
 *           type: string
 *           description: name of the student
 *         surname:
 *           type: string
 *           description: surname of the student
 *         email:
 *           type: string
 *           description: student's email
 *         age:
 *           type: number
 *           description: student's age
 *         groupId:
 *           type: string
 *           description: student's group ID
 *       example:
 *         name: Bob
 *         surname: Dewdney
 *         email: example@mail.com
 *         age: 21
 *         groupId: 1
 *
 */
