import { DataSourceOptions } from 'typeorm';

export const databaseConfiguration = (
    isMigrationRun = true
): DataSourceOptions => {
    const ROOT_PATH = process.cwd();
    const migrationPath = `${ROOT_PATH}/**/migrations/*{.ts,.js}`;
    const entitiesPath = `${ROOT_PATH}/**/*.entity{.ts,.js}`;

    return {
        type: 'postgres',
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        entities: [entitiesPath],
        migrations: [migrationPath],
        migrationsTableName: 'migrations',
        migrationsRun: isMigrationRun,
        logging: true,
        synchronize: true,
    };
};
