import express from 'express';
import * as courseController from './course.controller';
import controllerWrapper from '../app/utils/controller-wrapper';
import validator from '../app/middlewares/validation.middlevare';

import { idParamSchema } from '../app/schemas/id-param.schema';
import { courseCreateSchema, courseUpdateLectorsSchema } from './course.schema';

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The course managing API
 * /api/v1/courses:
 *    get:
 *      summary: Get list of courses
 *      tags: [Courses]
 *      description: Retrieves a list of all courses.
 *      responses:
 *        200:
 *          description: Successful operation
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                items:
 *                  $ref: '#/components/schemas/CourseResponse'
 *        404:
 *         description: Not Found
 *         content:
 *           text/html:
 *             schema:
 *               type: string
 *               example: Not Found
 *        500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.get('/', controllerWrapper(courseController.getAllCourses));

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The course managing API
 * /api/v1/courses:
 *   post:
 *     summary: Create a new course
 *     tags: [Courses]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateCourseRequest'
 *     responses:
 *       201:
 *         description: The successfully created course.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CourseResponse'
 *       400:
 *         description: Bad request
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Error validating request body. "name" | "description" | "hours" is required.
 *       409:
 *         description: Bad request
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Course already exist.
 *       500:
 *         description: Some server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: Internal server error
 *
 */
router.post(
    '/',
    validator.body(courseCreateSchema),
    controllerWrapper(courseController.addNewCourse)
);

// router.patch(
//     '/:id/lector',
//     validator.params(idParamSchema),
//     validator.body(courseUpdateLectorsSchema),
//     controllerWrapper(courseController.subscribeLectorToCourse)
// );

export default router;
