import express from 'express';
import * as courseService from './course.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ICourseCreateRequest } from './types/course-create-request.interface';
import { Course } from './entities/course.entity';
import { ICourse } from './types/course.interface';

const getAllCourses = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const courses: Course[] = await courseService.getAllCourses();

    res.status(200).json(courses);
};

const addNewCourse = async (
    req: ValidatedRequest<ICourseCreateRequest>,
    res: express.Response
): Promise<void> => {
    const data: Omit<ICourse, 'id'> = req.body;
    const newCourse: Course = await courseService.addNewCourse(data);

    res.status(201).json(newCourse);
};

const subscribeLectorToCourse = async (
    req: express.Request,
    res: express.Response
): Promise<void> => {
    const { lectorId } = req.body;
    const { id } = req.params;
    const courseId: number = +id;

    await courseService.subscribeLectorToCourse(courseId, lectorId);

    res.status(200).json('Lector successfylly added to the course');
};

export { getAllCourses, addNewCourse, subscribeLectorToCourse };
