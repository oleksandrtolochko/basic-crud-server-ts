import { Column, Entity, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { CoreEntity } from '../../app/entities/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Student } from '../../students/entities/student.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
    @Column({
        type: 'varchar',
    })
    name: string;

    @Column({
        type: 'varchar',
    })
    description: string;

    @Column({
        type: 'integer',
    })
    hours: number;

    @ManyToMany(() => Lector, (lector) => lector.courses)
    lectors: Lector[];

    @ManyToMany(() => Student, (student) => student.courses)
    @JoinTable({
        name: 'course_students',
        joinColumn: { name: 'course_id', referencedColumnName: 'id' },
        inverseJoinColumn: { name: 'student_id', referencedColumnName: 'id' },
    })
    students: Student[];

    @OneToMany(() => Mark, (mark) => mark.course)
    marks: Mark[];
}
