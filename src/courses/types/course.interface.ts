export interface ICourse {
    id: number;
    name: string;
    description: string;
    hours: number;
}

/**
 * @swagger
 * components:
 *  schemas:
 *    CourseResponse:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        createdAt:
 *          type: string
 *          format: date-time
 *        updatedAt:
 *          type: string
 *          format: date-time
 *        name:
 *          type: string
 *        description:
 *          type: string
 *        hours:
 *          type: number
 *      example:
 *        id: 1
 *        createdAT: "2023-07-15T14:46:52.072Z"
 *        updatedAT: "2023-07-15T14:46:52.072Z"
 *        name: 'English'
 *        description: 'foreign language'
 *        hours: 90
 *    CreateCourseRequest:
 *      type: object
 *      required:
 *        - name
 *        - description
 *        - hours
 *      properties:
 *        name:
 *          type: string
 *        description:
 *          type: string
 *        hours:
 *          type: number
 *      example:
 *        name: 'English'
 *        description: 'foreign language'
 *        hours: 90
 */
