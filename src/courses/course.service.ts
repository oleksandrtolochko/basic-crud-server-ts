import { UpdateResult } from 'typeorm';
import { HttpStatuses } from '../app/enums/http-statuses.enum';
import HttpExeption from '../app/exeptions/http-exeption';
import { AppDataSource } from '../configs/database/data-source';
import { Lector } from '../lectors/entities/lector.entity';
import { Course } from './entities/course.entity';
import { ICourse } from './types/course.interface';

const courseRepository = AppDataSource.getRepository(Course);
const lectorRepository = AppDataSource.getRepository(Lector);

const getAllCourses = async (): Promise<Course[]> => {
    const courses = await courseRepository.find({});
    return courses;
};

const addNewCourse = async (
    courseCreateSchema: Omit<ICourse, 'id'>
): Promise<Course> => {
    const course: Course | null = await courseRepository.findOneBy({
        name: courseCreateSchema.name,
    });

    if (course) {
        throw new HttpExeption(HttpStatuses.CONFLICT, 'Course already exist');
    }

    const newCourse: Omit<ICourse, 'id'> & Course = await courseRepository.save(
        courseCreateSchema
    );
    return newCourse;
};

const subscribeLectorToCourse = async (
    lectorId: number,
    courseId: number
): Promise<UpdateResult> => {
    const course: Course | null = await courseRepository.findOneBy({
        id: courseId,
    });

    if (!course) {
        throw new HttpExeption(HttpStatuses.NOT_FOUND, "Course doesn't exist");
    }

    const lector: Lector | null = await lectorRepository.findOneBy({
        id: lectorId,
    });

    if (!lector) {
        throw new HttpExeption(
            HttpStatuses.BAD_REQUEST,
            "Lector doesn't exist"
        );
    }

    const res: UpdateResult = await courseRepository
        .createQueryBuilder()
        .update(Course)
        .set({ lectors: () => `array_append(lectors, ${lectorId})` })
        .where('id = :id', { id: courseId })
        .execute();

    if (!res.affected) {
        throw new HttpExeption(
            HttpStatuses.INTERNAL_SERVER_ERROR,
            'Something went wrong'
        );
    }
    return res;
};

export { getAllCourses, addNewCourse, subscribeLectorToCourse };
