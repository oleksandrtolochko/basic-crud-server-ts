import { ILector } from '../lectors/types/lector.interface';
import { ICourse } from './types/course.interface';
import Joi from 'joi';

export const courseCreateSchema = Joi.object<Omit<ICourse, 'id'>>({
    name: Joi.string().required(),
    description: Joi.string().required(),
    hours: Joi.number().required(),
});

export const courseUpdateSchema = Joi.object<Partial<ICourse>>({
    name: Joi.string().optional(),
    description: Joi.string().optional(),
    hours: Joi.number().optional(),
});

export const courseUpdateLectorsSchema = Joi.object<Partial<ILector>>({
    id: Joi.number().required(),
});
