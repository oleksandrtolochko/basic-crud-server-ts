import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateRelationsBetweenStudentsAndCoursesTables1688325051483 implements MigrationInterface {
    name = 'CreateRelationsBetweenStudentsAndCoursesTables1688325051483'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "course_students" ("course_id" integer NOT NULL, "student_id" integer NOT NULL, CONSTRAINT "PK_4a364b5ed5add8d9d3c3edf0cbb" PRIMARY KEY ("course_id", "student_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_f92b04de7259dd40d646d0a457" ON "course_students" ("course_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_0c333793939e89fc835e6a95db" ON "course_students" ("student_id") `);
        await queryRunner.query(`ALTER TABLE "course_students" ADD CONSTRAINT "FK_f92b04de7259dd40d646d0a4570" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "course_students" ADD CONSTRAINT "FK_0c333793939e89fc835e6a95dbd" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "course_students" DROP CONSTRAINT "FK_0c333793939e89fc835e6a95dbd"`);
        await queryRunner.query(`ALTER TABLE "course_students" DROP CONSTRAINT "FK_f92b04de7259dd40d646d0a4570"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0c333793939e89fc835e6a95db"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_f92b04de7259dd40d646d0a457"`);
        await queryRunner.query(`DROP TABLE "course_students"`);
    }

}
