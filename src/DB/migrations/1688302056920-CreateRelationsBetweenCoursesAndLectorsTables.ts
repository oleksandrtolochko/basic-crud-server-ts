import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateRelationsBetweenCoursesAndLectorsTables1688302056920 implements MigrationInterface {
    name = 'CreateRelationsBetweenCoursesAndLectorsTables1688302056920'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "courses_lectors_lectors" ("coursesId" integer NOT NULL, "lectorsId" integer NOT NULL, CONSTRAINT "PK_8cb2d5f1a586b1cc7b3e84671d2" PRIMARY KEY ("coursesId", "lectorsId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_ec00005e31c98cb62be97fee2b" ON "courses_lectors_lectors" ("coursesId") `);
        await queryRunner.query(`CREATE INDEX "IDX_925541015bf4eb86fb79f3b7fa" ON "courses_lectors_lectors" ("lectorsId") `);
        await queryRunner.query(`ALTER TABLE "courses_lectors_lectors" ADD CONSTRAINT "FK_ec00005e31c98cb62be97fee2b6" FOREIGN KEY ("coursesId") REFERENCES "courses"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "courses_lectors_lectors" ADD CONSTRAINT "FK_925541015bf4eb86fb79f3b7fac" FOREIGN KEY ("lectorsId") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "courses_lectors_lectors" DROP CONSTRAINT "FK_925541015bf4eb86fb79f3b7fac"`);
        await queryRunner.query(`ALTER TABLE "courses_lectors_lectors" DROP CONSTRAINT "FK_ec00005e31c98cb62be97fee2b6"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_925541015bf4eb86fb79f3b7fa"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ec00005e31c98cb62be97fee2b"`);
        await queryRunner.query(`DROP TABLE "courses_lectors_lectors"`);
    }

}
